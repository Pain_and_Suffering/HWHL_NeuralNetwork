from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.optimizers import adadelta
from keras.utils import np_utils
from keras import losses

import numpy as np
import os
from PIL import Image
from numpy import *

from sklearn.cross_validation import train_test_split

img_rows, img_cols = 77, 77
img_channels = 1

#path2='..\..\PythonApplication2\PythonApplication2\preprocessed2'
path2 = ".\input_data"
imlist = os.listdir(path2)

print("Data is loading. Please, wait...")
frames = []
for im2 in imlist:
    im = Image.open(path2+ '\\' + im2).convert('L')
    
    imNorm = [(255 - x) * 1.0 / 255.0 for x in im.getdata()]
    frames.append(imNorm)
frames = np.array(frames)

hwhls=np.genfromtxt("HWHLs", usecols=[0], unpack=True) #значения ширин для обучающей выборки
#lis=hwhls[0:5000]

#frames = np.array([ frames[0] for x in frames ])
#lis = np.array([hwhls[x] for x in range(len(hwhls))])


X_train, X_test, y_train, y_test = train_test_split(frames, hwhls, test_size=0.2, random_state=2)

print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')
y_size=y_train.shape[0]

model = Sequential()
model.add(Dense(200, input_shape=(6084,)))
model.add(Activation('relu'))
model.add(Dense(100))
model.add(Activation('linear'))
model.add(Dropout(0.2))
model.add(Dense(1))

adadelta(lr=0.001)
model.compile('adadelta', 'mse')

model.fit(X_train, y_train, batch_size=32, epochs=150, verbose=1)
score = model.evaluate(X_test, y_test, verbose=0)
print('Test loss:', score)
print(model.summary())
print(model.predict(X_test[0:5]))
print(y_test[0:5])
