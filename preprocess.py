###############################
#
# Препроцессинг данных
#
###############################


import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import os
from PIL import Image
from numpy import *

img_rows, img_cols = 77, 77
img_channels = 1

path1=".\input_data"
path2='.\preprocessed2'

listing = os.listdir(path1) 
num_samples=size(listing)

for file in listing:
    im = Image.open(path1 + '\\' + file)   
    img = im.resize((img_rows,img_cols))
    gray = img.convert('L')  
    gray.save(path2 +'\\' +  file, "JPEG")

    #надо полосу загрузки сделать, а то не понятно, прога вообще жива